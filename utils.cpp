#include "utils.hpp"

extern std::unordered_map<std::uint16_t, std::uint16_t> card_id_to_index;

void Deck::build_deck(const uint16_t seed, const uint8_t size_deck) {
	const Graph &g = *graph;
	this->clear();
	//TODO : Function to build deck*
	this->cards.resize(size_deck);
	this->weight.resize(g.m_vertex_set.size());
	this->cards.at(0) = card_id_to_index.at(seed);

	#pragma omp parallel for
	for (std::size_t j = 0; j < g.m_vertex_set.size(); ++j) {
		weight[j] = get(boost::edge_weight_t(), g, boost::edge(card_id_to_index.at(seed), j, g).first);
	}

	for (std::size_t k = 1; k < size_deck; ++k) {
		this->cards.at(k) = std::distance(weight.cbegin(), std::max_element(weight.cbegin(), weight.cend()));
		sync_weight(this->cards.at(k));
	}
}

void Deck::sync_weight(const uint8_t index) {
	const Graph &g = *graph;
	#pragma omp parallel for
	for (std::size_t j = 0; j < g.m_vertex_set.size(); ++j) {
		if (index == j) {
			this->weight[j] *= 0.2f;
			continue;
		}
		this->weight[j] = (this->weight[j] + (get(boost::edge_weight_t(), g, boost::edge(index, j, g).first) * 0.2f)) * 0.5f;
	}
}

void Deck::clear() {
	this->cards.clear();
	this->weight.clear();
	this->size = 0;
}

std::ostream &operator<<(std::ostream &os, const Deck &deck) {
	os << "MultiverseID,Count,Foil,Tag\n";
	for (const auto &item : deck.cards) {
		const auto tmp = (*deck.graph)[item].multiverse_id;
		os << '"' << tmp << R"(","1","","Deck")" << '\n';
	}
	return os;
}

float weight::compute_weight(const Card &c1, const Card &c2) {
	//TODO : Function to compute weight between two cards
	const auto color = color_weight(c1, c2);
	const auto capacity_ = capacity_weight(c1, c2);
	const auto supertype = supertype_weight(c1, c2);
	const auto type = type_weight(c1, c2);
	const auto subtype = subtype_weight(c1, c2);
	const auto tmp = (color + (0.8f * capacity_) + (0.15f * supertype) + (0.5f * type) + (0.15f * subtype));
	return tmp;
}