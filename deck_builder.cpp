#include "deck_builder.hpp"
#include <fstream>

DeckBuilder::DeckBuilder(const std::filesystem::path &pathToDb, const std::filesystem::path &pathToHelper, const std::size_t amountOfCards) {
	const std::string bdd = [&]() -> std::string {
		std::ifstream file{ pathToDb };
		std::stringstream stream;
		stream << file.rdbuf();
		return stream.str();
	}();
	const std::string helper = [&]() -> std::string {
		std::ifstream file{ pathToHelper };
		std::stringstream stream;
		stream << file.rdbuf();
		return stream.str();
	}();
	json_parser(bdd, helper, amountOfCards);
}

void DeckBuilder::json_parser(const std::string &bddJsonStr, const std::string &nbJsonHelperStr, const std::size_t amountOfCard) {
	const auto[bdd, helper] = [&]() -> std::pair<json, json> {
		simdjson::dom::parser parser;
		return { parser.load(bddJsonStr), parser.load(nbJsonHelperStr) };
	}();
//	const json bdd = json::parse(bddJsonStr), helper = json::parse(nbJsonHelperStr);
	const std::size_t nb_capacity = /*std::stoul(*/helper.at(0)["count"].get_uint64()/*)*/,
			nb_legality = /*std::stoul(*/helper.at(1)["count"].get_uint64()/*)*/,
			nb_supertype = /*std::stoul(*/helper.at(2)["count"].get_uint64()/*)*/,
			nb_type = /*std::stoul(*/helper.at(3)["count"].get_uint64()/*)*/,
			nb_subtype = /*std::stoul(*/helper.at(4)["count"].get_uint64()/*)*/;
	cards = Graph(amountOfCard);
	{
//		std::size_t i = 0;
		auto bdd_iterator = bdd.begin();
		for (auto[vi, vi_end] = boost::vertices(cards); vi != vi_end; ++vi, ++bdd_iterator) {
			const auto &current_bdd_element = *bdd_iterator;
			const auto layout = str2int(current_bdd_element["cla_name"].get_c_str());
			const std::size_t first_mana_cost = /*std::stoul(*/current_bdd_element["car_convertedmanacost"].get_uint64()/*)*/;

			cards[*vi].capacity.resize(nb_capacity);
			cards[*vi].legality.resize(nb_legality);
			cards[*vi].supertype.resize(nb_supertype);
			cards[*vi].type.resize(nb_type);
			cards[*vi].subtype.resize(nb_subtype);

			switch (layout) {
				case str2int("split"):
				case str2int("flip"):
				case str2int("double-faced"):
				case str2int("aftermath"): {
					const auto &next_bdd_element = [&]() -> element {
						auto next_iter = bdd_iterator;
						++next_iter;
						return *next_iter;
					}();
					const std::size_t second_mana_cost = /*std::stoul(*/next_bdd_element["car_convertedmanacost"].get_uint64()/*)*/;
					// "fusion" of cards.
					if (first_mana_cost >= second_mana_cost) {
						fill_card(cards[*vi], current_bdd_element, layout, first_mana_cost);
					} else {
						fill_card(cards[*vi], next_bdd_element, layout, second_mana_cost);
					}
					cards[*vi].colour_identity = convert_colour(current_bdd_element["car_coloridentity"].get<std::string_view>());
					++bdd_iterator;
					cards[*vi].colour_identity |= convert_colour((*bdd_iterator)["car_coloridentity"].get<std::string_view>());

					// If the card cannot be inserted and it does not exist, it fails. Otherwise, it is all good.
					if (!card_id_to_index.emplace(cards[*vi].card_id, *vi).second &&
						card_id_to_index.find(cards[*vi].card_id) == card_id_to_index.cend()) {
						throw std::runtime_error("Impossible to insert the card_id in the hash table.");
					}
				}
					break;

				default:
					fill_card(cards[*vi], current_bdd_element, layout, first_mana_cost);
					cards[*vi].colour_identity = convert_colour(current_bdd_element["car_coloridentity"].get<std::string_view>());

					// If the card cannot be inserted and it does not exist, it fails. Otherwise, it is all good.
					if (!card_id_to_index.emplace(cards[*vi].card_id, *vi).second &&
						card_id_to_index.find(cards[*vi].card_id) == card_id_to_index.cend()) {
						throw std::runtime_error("Impossible to insert the card_id in the hash table.");
					}
					break;
			}
		}
	}
}