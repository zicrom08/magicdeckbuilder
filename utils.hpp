#ifndef MAGICDECKBUILDER_UTILS_HPP
#define MAGICDECKBUILDER_UTILS_HPP

#include <vector>
#include <cstdint>
#include <boost/graph/adjacency_matrix.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/dynamic_bitset.hpp>
#include <ostream>

/**
 * \brief This structure represents the vertices inside the graph and also a card from Magic The Gathering.
 * \struct Card
 */
struct Card {
	//! Order of colours : White, Blue, Black, Red, Green, Colourless
	std::bitset<6> colour_identity;
	boost::dynamic_bitset<> capacity;
	boost::dynamic_bitset<> legality;
	boost::dynamic_bitset<> supertype;
	boost::dynamic_bitset<> type;
	boost::dynamic_bitset<> subtype;

	std::uint32_t multiverse_id{ 0 };
	std::uint16_t card_id{ 0 };
	std::uint8_t mana_cost{ 0 };
	std::uint8_t block{ 0 };
	std::uint8_t layout{ 0 };

	std::int8_t power{ 0 };
	std::int8_t toughness{ 0 };

	Card() = default;

	Card(const Card &card) = default;

	Card(Card &&card) noexcept {
		if (this != &card) {
			colour_identity = card.colour_identity;
			capacity = std::move(card.capacity);
			legality = std::move(card.legality);
			supertype = std::move(card.supertype);
			type = std::move(card.type);
			subtype = std::move(card.subtype);

			multiverse_id = card.multiverse_id;
			card_id = card.card_id;
			mana_cost = card.mana_cost;
			block = card.block;
			layout = card.layout;
			power = card.power;
			toughness = card.toughness;
		}
	}

	Card &operator=(const Card &card) = default;

	Card &operator=(Card &&card) noexcept {
		if (this != &card) {
			colour_identity = card.colour_identity;
			capacity = std::move(card.capacity);
			legality = std::move(card.legality);
			supertype = std::move(card.supertype);
			type = std::move(card.type);
			subtype = std::move(card.subtype);

			multiverse_id = card.multiverse_id;
			card_id = card.card_id;
			mana_cost = card.mana_cost;
			block = card.block;
			layout = card.layout;
			power = card.power;
			toughness = card.toughness;
		}
		return *this;
	}
};

using Graph = boost::adjacency_matrix<boost::undirectedS, Card, boost::property<boost::edge_weight_t, float>>;

/**
 * \brief This is the structure which represents a deck from Magic The Gathering.
 * \struct Deck
 */
struct Deck {
	std::vector<uint16_t> cards;
	std::vector<float> weight;
	std::uint8_t size{ 0 };
	const Graph *graph{ nullptr };

	Deck() = default;

	explicit Deck(const Graph &graph) : graph(&graph) {}

	/*Deck(const std::vector<uint16_t> &cards,
		 const std::vector<float> &weight,
		 const std::uint8_t &size,
		 const Graph &graph) :
			cards(cards),
			weight(weight),
			size(size),
			graph(&graph) {}*/

	Deck(const Deck &deck) = default;

	Deck(Deck &&deck) noexcept {
		cards = std::move(deck.cards);
		weight = std::move(deck.weight);
		size = deck.size;
		graph = deck.graph;
		deck.size = 0;
		deck.graph = nullptr;
	}

	Deck &operator=(const Deck &deck) = default;

	Deck &operator=(Deck &&deck) noexcept {
		if (this != &deck) {
			cards = std::move(deck.cards);
			weight = std::move(deck.weight);
			size = deck.size;
			graph = deck.graph;
		}
		return *this;
	}

	void build_deck(const uint16_t seed, const uint8_t size_deck);

	void sync_weight(const uint8_t index);

	/**
	 * \brief It clears all the vectors inside the deck.
	 */
	void clear();

	/**
	 * \brief It exports the deck in CSV format understandable for UrzaGatherer.
	 * \param os
	 * \param deck
	 * \return
	 */
	friend std::ostream &operator<<(std::ostream &os, const Deck &deck);
};

namespace weight {
	[[gnu::always_inline]] inline float color_weight(const Card &c1, const Card &c2) {
		const auto result = c1.colour_identity & c2.colour_identity;
		return (c1.colour_identity.count() > c2.colour_identity.count() ?
				static_cast<float>(result.count()) / c1.colour_identity.count() :
				static_cast<float>(result.count()) / c2.colour_identity.count());
	}

	[[gnu::always_inline]] inline float capacity_weight(const Card &c1, const Card &c2) {
		const auto result = c1.capacity & c2.capacity;
		if (result.count() == 0)
			return 0.f;
		return (c1.capacity.count() > c2.capacity.count() ?
				static_cast<float>(result.count()) / c1.capacity.count() :
				static_cast<float>(result.count()) / c2.capacity.count());
	}

	[[gnu::always_inline]] inline float supertype_weight(const Card &c1, const Card &c2) {
		const auto result = c1.supertype & c2.supertype;
		if (result.count() == 0)
			return 0.f;
		return (c1.supertype.count() > c2.supertype.count() ?
				static_cast<float>(result.count()) / c1.supertype.count() :
				static_cast<float>(result.count()) / c2.supertype.count());
	}

	[[gnu::always_inline]] inline float type_weight(const Card &c1, const Card &c2) {
		const auto result = c1.type & c2.type;
		if (result.count() == 0)
			return 0.f;
		return (c1.type.count() > c2.type.count() ?
				static_cast<float>(result.count()) / c1.type.count() :
				static_cast<float>(result.count()) / c2.type.count());
	}

	[[gnu::always_inline]] inline float subtype_weight(const Card &c1, const Card &c2) {
		const auto result = c1.subtype & c2.subtype;
		if (result.count() == 0)
			return 0.f;
		return (c1.subtype.count() > c2.subtype.count() ?
				static_cast<float>(result.count()) / c1.subtype.count() :
				static_cast<float>(result.count()) / c2.subtype.count());
	}

	float compute_weight(const Card &c1, const Card &c2);
}

#endif //MAGICDECKBUILDER_UTILS_HPP
