# MagicDeckBuilder  [![pipeline status](https://gitlab.com/zicrom08/magicdeckbuilder/badges/master/pipeline.svg)](https://gitlab.com/zicrom08/magicdeckbuilder/-/commits/master) [![coverage report](https://gitlab.com/zicrom08/magicdeckbuilder/badges/master/coverage.svg)](https://gitlab.com/zicrom08/magicdeckbuilder/-/commits/master)

## Introduction

The purpose of this application is to build a coherent Magic The Gathering deck from a database of all the cards. To do so, we will use graph theory
to model the problem and apply some operational research algorithms. This application is a French university project in High Performance Computing. We
are 4 students to carry out this project.

## Dependencies

* We use the boost library to manage the graph.
```bash
sudo apt-get update && sudo apt-get install pkg-config libboost-dev cmake build-essential
```
* To use it in PHP, we use [PHP-CPP](https://www.php-cpp.com/)

To install it, you can click [here](https://www.php-cpp.com/documentation/install)

## Installation
To use it as a PHP extension, you have to copy-paste the file `MTGDeckBuilder.ini`
in your config file for PHP extensions. To learn more, or to be sure of it, please click [here](https://www.php-cpp.com/documentation/your-first-extension).