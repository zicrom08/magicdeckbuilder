#ifndef MAGICDECKBUILDER_DECK_BUILDER_HPP
#define MAGICDECKBUILDER_DECK_BUILDER_HPP

#include "utils.hpp"
#include <filesystem>
#include <unordered_map>
#include <charconv>
#include <boost/algorithm/string.hpp>
#include <simdjson.h>

using json = simdjson::simdjson_result<simdjson::dom::element>;
using simdjson::dom::element;

/**
 * \brief This constexpr function is used to convert a string to an int.
 *
 * It hashes the string. The constexpr is very useful : for a const
 *
 * \param str
 * \param h
 * \return
 */
constexpr unsigned int str2int(const char *str, const int h = 0) {
	return !str[h] ? 5381 : (str2int(str, h + 1) * 33) ^ str[h];
}

class DeckBuilder {
protected:
	Graph cards{ 0 };

	Deck deck;

	/**
	 * \brief This builds the graph. This is the most compute intensive part of the whole program
	 */
	[[gnu::always_inline]] inline void buildGraph() {
		auto taille = cards.m_vertex_set.size();
		for (size_t j = taille - 1; j > 0; --j) {
			for (size_t k = 0; k < taille; ++k) {
				boost::add_edge(j, k, std::numeric_limits<float>::min(), cards);
			}
			--taille;
		}

		taille = cards.m_vertex_set.size();
		for (size_t j = taille - 1; j > 0; --j) {
#pragma omp parallel for schedule(guided)
			for (size_t k = 0; k < taille; ++k) {
				cards.get_edge(j, k).second.m_value = weight::compute_weight(cards[j], cards[k]);
			}
			--taille;
		}
	}

	/**
	 * \brief It is used to parse the Database and other files that are in JSON format, given directly by PHP.
	 * \param[in] bddJsonStr Database
	 * \param[in] nbJsonHelperStr Helper to parse
	 * \param[in] amountOfCard
	 */
	void json_parser(const std::string &bddJsonStr, const std::string &nbJsonHelperStr, const std::size_t amountOfCard);

	/**
	 * \brief This fucntion was made to be able to convert a field from the Card structure.
	 * \tparam CardContainer
	 * \param[in,out] container
	 * \param[in] card
	 * \param[in] str
	 */
	template<typename CardContainer>
	[[gnu::always_inline]] inline void convert_field(CardContainer &container, const element &card, const std::string &str) {
		if (card[str].is_null()) {
			return;
		}
		std::vector<std::string> split_vec;
		boost::split(split_vec, card[str].get_string().value_unsafe(), boost::is_any_of(";"), boost::algorithm::token_compress_on);
		std::for_each(split_vec.cbegin(), split_vec.cend(), [&](const std::string &toconvert) -> void {
			container[std::stoul(toconvert)] = true;
		});
	}

	/**
	 * \brief It is used to convert frmo the JSON file to the Card structure for the colour.
	 * \param colour_identity
	 * \return
	 */
	[[gnu::always_inline]] static inline decltype(Card::colour_identity) convert_colour(const std::string_view &colour_identity) {
		std::vector<std::string> split_vec;
		decltype(Card::colour_identity) colour; // All bits are set to 0  : default constructor.
		if (!colour_identity.empty()) {
			boost::split(split_vec, colour_identity, boost::is_any_of(";"));
			for (const auto &letter: split_vec) {
				switch (letter[0]) {
					case 'W':
						colour[0] = true;
						break;
					case 'U':
						colour[1] = true;
						break;
					case 'B':
						colour[2] = true;
						break;
					case 'R':
						colour[3] = true;
						break;
					case 'G':
						colour[4] = true;
						break;
					default:
						throw std::runtime_error("Default case in conversion of colour identity. Normally impossible.");
				}
			}
		} else {
			colour[5] = true;
		}
		return colour;
	}

	/**
	 * \brief Used to fill the fields of the card.
	 * \param card
	 * \param bddCard
	 * \param layout
	 * \param mana_cost
	 */
	[[gnu::always_inline]] inline void fill_card(Card &card, const element &bddCard, const std::size_t layout, const std::size_t mana_cost) {
		{
			const std::string_view val = bddCard["sca_multiverseid"].get_string();
			std::from_chars(val.data(), val.data() + val.size(), card.multiverse_id);
		}
		{
			const std::string_view val = bddCard["car_id"].get_string();
			std::from_chars(val.data(), val.data() + val.size(), card.card_id);
		}
		card.mana_cost = mana_cost;
		{
			const std::string_view val = bddCard["set_block"].get_string();
			std::from_chars(val.data(), val.data() + val.size(), card.block);
		}
		card.layout = layout;
		{
			const std::string_view val = bddCard["car_power"].get_string();
			std::from_chars(val.data(), val.data() + val.size(), card.power);
		}
		{
			const std::string_view val = bddCard["car_toughness"].get_string();
			std::from_chars(val.data(), val.data() + val.size(), card.toughness);
		}

		convert_field(card.legality, bddCard, "clel_legality");
		convert_field(card.subtype, bddCard, "cstl_subtype");
		convert_field(card.supertype, bddCard, "csul_supertype");
		convert_field(card.type, bddCard, "ctyl_type");
		convert_field(card.capacity, bddCard, "ccal_capacity");
	}

public:
	DeckBuilder() = default;

	virtual ~DeckBuilder() = default;

	explicit DeckBuilder(const std::string &db, const std::string &helper, const std::size_t amountOfCards) {
		json_parser(db, helper, amountOfCards);
	}

	explicit DeckBuilder(const std::filesystem::path &pathToDb, const std::filesystem::path &pathToHelper, const std::size_t amountOfCards);

	//! it used as a corresponding table for the index for the graph and the id of the card.
	std::unordered_map<std::uint16_t, std::uint16_t> card_id_to_index;
};


#endif //MAGICDECKBUILDER_DECK_BUILDER_HPP
